# CTU BUSINESS CARD

## REQUIREMENTS 

- LuaLaTeX
- Inkscape
- Installed Technica Bold font

## USED PACKAGES
- geometry
- babel
- fontenc
- tikz
- ifthen
- svg
- fontspec
- xcolor
- qrcode
- eso-pic
- calc
- fp
- microtype

## USAGE

1. Edit the file according to the instructions inside the file.
    - _Optional: to add your own QR code, just fill the desired link in `\qrlink`. Leave empty if your don't want QR code on bussiness card._
2. Create documment by executing this command
```shell
lualatex --shell-escape business-cards.tex
```


